import re


def check(info: list) -> bool:
    if not info[0]:
        return False
    if not info[1]:
        return False
    if not info[2]:
        return False
    if not info[3]:
        return False
    if not re.search(r'\w*@stud.kpfu.ru', info[4]):
        return False
    if not info[5]:
        return False
    return True


def indexing(i, iterator) -> bool:
    if i == 0 and (iterator[0] == 'Сведения о собрании' or iterator[0] == 'Meeting Summary'):
        return True
    elif i == 1 and (
            iterator[0] == 'Total Number of Participants' or iterator[0] == 'Общее количество участников'):
        return True
    elif i == 2 and (iterator[0] == 'Название собрания' or iterator[0] == 'Meeting Title'):
        return True
    elif i == 3 and (iterator[0] == 'Meeting Start Time' or iterator[0] == 'Время начала собрания'):
        return True
    elif i == 4 and (iterator[0] == 'Конец собрания' or iterator[0] == 'Meeting End Time'):
        return True
    elif i == 5:
        return True
    elif i == 6 and (iterator[0] == 'Полное имя' or iterator[0] == 'Full Name'):
        return True
    else:
        return False
