# https://docs.python.org/3/library/csv.html
# Адельчик топчик
# Соболезнуем Аделе

import csv
import re
import sys
from datetime import datetime
from collections import defaultdict
import broken_table
import time_format
import zasranci

dict_sheets = defaultdict(list)
medium_input = []
medium_output = []
presence = []

with open('sheeting_2.csv', mode='r', encoding='utf-16') as f:
    reader = csv.reader(f, delimiter='\t', quotechar='|')
    for index, iterator in enumerate(reader):
        if 0 <= index <= 6 and broken_table.indexing(index, iterator):
            continue
        elif index > 6:
            if not broken_table.check(iterator):
                print('Error: broken table')
                sys.exit()
            string_one: str = re.sub(r'[",]*', '', iterator[1])
            string_two: str = re.sub(r'[",]*', '', iterator[2])
            dict_sheets[iterator[0]].append(string_one)
            dict_sheets[iterator[0]].append(string_two)
            medium_1, medium_2 = time_format.times(string_one, string_two)
            if not medium_1:
                print('Error: broken table time, ну или вы слишком замороченную дату дали(второе более вероятное')
                sys.exit()
            medium_input.append(medium_1)
            medium_output.append(medium_2)
        else:
            print('broken table')
            sys.exit()
    #
    # for key, value in dict_sheets.items():
    #     print(f'key: {key} value: {value}')

    medium_input = sorted(medium_input)[len(medium_input) // 2]
    medium_output = sorted(medium_output)[len(medium_output) // 2]
    average_class_time = medium_output - medium_input
    print(f'Сколько времени шло собрание - {average_class_time}')
    # print(f'{medium_input} : {medium_output}')

    for key, value in dict_sheets.items():
        print(f'{key} : {value}')

        if len(value) > 2:
            value[0], value[1] = zasranci.bad_students(value)

        input_time, output_time = time_format.times(value[0], value[1])

        input_time_student: datetime
        if input_time > medium_input:
            input_time_student = input_time
        else:
            input_time_student = medium_input
        if output_time < medium_output:
            output_time_student = output_time
        else:
            output_time_student = medium_output
        time_student = abs(output_time_student - input_time_student)
        # print(time_student)
        if len(value) < 3:
            dict_sheets[key].append(time_student)
        else:
            value[2] = time_student * (len(value) // 2)
        presence.append(f'{key} {str(time_student)}')

    print(f"Топ присутствующих в отсортированном виде - {sorted(presence)}")
    print(f'Введите сколько процентов по вашему человек должен был отсидеть на паре ', end='')
    percentage_of_presence = float(input())
    if -1 < percentage_of_presence < 101:
        for key, value in dict_sheets.items():
            # print(f'Выводим словарь - {key} : {value[2]}')
            if value[2] > (percentage_of_presence / 100) * average_class_time:
                print(f'{key} отсидел {percentage_of_presence}% собрания')
            else:
                print(f'{key} прогульщик')
    else:
        print('Ты не то ввёл, Искандер')
        sys.exit()

# It is my LEGENDARY Peterinskoe formulary
# начало 12:00
#
# Данис зашёл - 12:00
# Данис вышел - 13:00
#
# Снова зашёл - 15:00
# Снова вышел - 16:00
#
# Снова зашёл 14:00
# Снова вышел 14:30
#
#
# конец 18:00
#
# сред время захода (12+15+14)/2 = 13.67
# сред время выхода (13+16+14.5)/2 = 14.5
# Сред время пребывания 14.5 - 13.5 = 1 * 2 = 2
# Сред время пребывания 14.5 - 13.67 = 0.83 * 3 = 2.49
#
# 2021-03-31 11:52:56
# 2021-03-31 13:01:53
#
# 2021-03-31 13:01:53
# 2021-03-31 13:01:53
