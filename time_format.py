from datetime import datetime
import re


# !!!Внимание!!! РЕГУЛЯРКИ САМЫЕ ТУПЫЕ И ПРИМИТЫВНЫЕ, В СЛУЧАЕ ЧЕГО ПРИДЁТСЯ ИХ ПРОКАЧИВАТЬ
def times(str_1: str, str_2: str) -> [tuple[datetime, datetime], bool]:
    t_1: datetime
    t_2: datetime
    if re.match(r'(0[1-9]|1[0-9]|2[0-9]|3[01])\.(0[1-9]|1[012])\.[0-9]{4}\s[0-9]{2}:[0-9]{2}:[0-9]{2}', str_1):
        t_1 = datetime.strptime(str_1, '%d.%m.%Y %H:%M:%S')
        t_2 = datetime.strptime(str_2, '%d.%m.%Y %H:%M:%S')
        return t_1, t_2
    if re.match(r'(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/([0-9]{4})\s([0-9]{2}):([0-9]{2}):([0-9]{2})', str_1):
        t_1 = datetime.strptime(str_1, '%d/%m/%Y %H:%M:%S')
        t_2 = datetime.strptime(str_2, '%d/%m/%Y %H:%M:%S')
        return t_1, t_2
    if re.match(r'[0-9]{1-2}/[0-9]{2}/[0-9]{4}\s[0-9]{2}:[0-9]{2}:[0-9]{2}\sAM|PM|am|pm', str_1):
        t_1 = datetime.strptime(str_1, '%m/%d/%Y %I:%M:%S %p')
        t_2 = datetime.strptime(str_2, '%m/%d/%Y %I:%M:%S %p')
        return t_1, t_2
    # if re.match(r'[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}:[0-9]{2}', str_1):
    #     t_1 = datetime.strptime(str_1, '%m.%d.%Y %I:%M:%S %p')
    #     t_2 = datetime.strptime(str_2, '%m.%d.%Y %I:%M:%S %p')
    #     return t_1, t_2

    return False, False
